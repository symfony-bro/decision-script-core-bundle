<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\EventDispatcher;


use SymfonyBro\DecisionScriptCoreBundle\Model\QuestionInterface;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptContextInterface;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptResultInterface;

class QuestionAnnounceEvent extends ScriptEvent
{
    /**
     * @var QuestionInterface
     */
    private $question;

    /**
     * QuestionAnnounceEvent constructor.
     * @param ScriptContextInterface $context
     * @param ScriptResultInterface $scriptResult
     * @param QuestionInterface $question
     */
    public function __construct(ScriptContextInterface $context, ScriptResultInterface $scriptResult, QuestionInterface $question)
    {
        parent::__construct($context, $scriptResult);
        $this->question = $question;
    }

    /**
     * @return QuestionInterface
     */
    public function getQuestion(): QuestionInterface
    {
        return $this->question;
    }
}
