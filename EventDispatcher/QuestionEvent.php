<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\EventDispatcher;


use SymfonyBro\DecisionScriptCoreBundle\Model\QuestionResultInterface;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptContextInterface;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptResultInterface;

class QuestionEvent extends ScriptEvent
{
    /**
     * @var QuestionResultInterface
     */
    private $questionResult;

    /**
     * QuestionEvent constructor.
     * @param ScriptContextInterface $context
     * @param ScriptResultInterface $scriptResult
     * @param QuestionResultInterface $questionResult
     */
    public function __construct(ScriptContextInterface $context, ScriptResultInterface $scriptResult, QuestionResultInterface $questionResult)
    {
        parent::__construct($context, $scriptResult);
        $this->questionResult = $questionResult;
    }

    /**
     * @return QuestionResultInterface
     */
    public function getQuestionResult(): QuestionResultInterface
    {
        return $this->questionResult;
    }

}
