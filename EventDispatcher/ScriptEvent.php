<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\EventDispatcher;


use Symfony\Component\EventDispatcher\Event;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptContextInterface;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptResultInterface;

class ScriptEvent extends Event
{
    /**
     * @var ScriptResultInterface
     */
    private $scriptResult;
    /**
     * @var ScriptContextInterface
     */
    private $context;

    /**
     * ScriptEvent constructor.
     * @param ScriptContextInterface $context
     * @param ScriptResultInterface $scriptResult
     */
    public function __construct(ScriptContextInterface $context, ScriptResultInterface $scriptResult)
    {
        $this->scriptResult = $scriptResult;
        $this->context = $context;
    }

    /**
     * @return ScriptResultInterface
     */
    public function getScriptResult(): ScriptResultInterface
    {
        return $this->scriptResult;
    }

    /**
     * @return ScriptContextInterface
     */
    public function getContext(): ScriptContextInterface
    {
        return $this->context;
    }
}
