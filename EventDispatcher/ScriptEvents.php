<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\EventDispatcher;


final class ScriptEvents
{
    const SCRIPT_BEFORE_START = 'symfony_bro.script.before_start';
    const SCRIPT_STARTED = 'symfony_bro.script.started';

    const SCRIPT_BEFORE_RESTART = 'symfony_bro.script.before_restart';
    const SCRIPT_RESTARTED = 'symfony_bro.script.restarted';

    const QUESTION_ANNOUNCE = 'symfony_bro.script.question_announce';
    const QUESTION_ANNOUNCED = 'symfony_bro.script.question_announced';
    const QUESTION_ANSWERED = 'symfony_bro.script.question_answered';

    const SCRIPT_BEFORE_FINISH = 'symfony_bro.script.before_finish';
    const SCRIPT_FINISHED = 'symfony_bro.script.finished';
}
