<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\EventDispatcher;


use Symfony\Component\EventDispatcher\Event;
use SymfonyBro\DecisionScriptCoreBundle\Model\ScriptResultInterface;

class ScriptRestartEvent extends Event
{
    /**
     * @var ScriptResultInterface
     */
    private $scriptResult;

    /**
     * ScriptEvent constructor.
     * @param ScriptResultInterface $scriptResult
     */
    public function __construct(ScriptResultInterface $scriptResult)
    {
        $this->scriptResult = $scriptResult;
    }

    /**
     * @return ScriptResultInterface
     */
    public function getScriptResult(): ScriptResultInterface
    {
        return $this->scriptResult;
    }
}
