<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\ExpressionLanguage;


use Money\Currency;
use Money\Money;
use RuntimeException;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;

class ExpressionLanguageScriptExtension implements ExpressionFunctionProviderInterface
{

    public function getFunctions()
    {
        return [
            new ExpressionFunction('choose', function ($result, $variants, $default = false) {
                throw new RuntimeException('"choose" function does not support compilation!');

            }, function ($arguments, $result, $variants, $default = false) {
                if(is_array($result)) {
                    // multiple
                    if(0 === $next = array_search($result, $variants, true)) {
                        $next = false;
                    } elseif (false === $next) {
                        $next = $default;
                    }
                } else {
                    // single
                    $next = $variants[$result]??$default;
                }

                return $next;
            }),
            new ExpressionFunction('money', function ($result, $variants, $default = false) {
                throw new RuntimeException('"money" function does not support compilation!');

            }, function ($arguments, $amount, $currencyCode = 'RUB') {
                return new Money($amount, new Currency($currencyCode));
            })
        ];
    }
}
