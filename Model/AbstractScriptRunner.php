<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use SymfonyBro\DecisionScriptCoreBundle\EventDispatcher\QuestionAnnounceEvent;
use SymfonyBro\DecisionScriptCoreBundle\EventDispatcher\QuestionEvent;
use SymfonyBro\DecisionScriptCoreBundle\EventDispatcher\ScriptEvent;
use SymfonyBro\DecisionScriptCoreBundle\EventDispatcher\ScriptEvents;
use SymfonyBro\DecisionScriptCoreBundle\EventDispatcher\ScriptRestartEvent;

abstract class AbstractScriptRunner implements ScriptRunnerInterface
{
    /**
     * @var ScriptResultStorageInterface
     */
    private $storage;

    /**
     * @var QuestionDeciderInterface
     */
    private $decider;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AbstractScriptRunner constructor.
     * @param ScriptResultStorageInterface $storage
     * @param QuestionDeciderInterface $decider
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(ScriptResultStorageInterface $storage, QuestionDeciderInterface $decider, EventDispatcherInterface $eventDispatcher)
    {
        $this->storage = $storage;
        $this->decider = $decider;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Script manager creates new ScriptResult,
     * find entry question,
     * create for it QuestionResult and add to ScriptResult and persists all
     *
     * @param ScriptInterface $script
     * @param ScriptContextInterface $context
     * @return ScriptResultInterface
     * @throws LogicException
     */
    public function start(ScriptInterface $script, ScriptContextInterface $context): ScriptResultInterface
    {
        $scriptResult = $this->createScriptResult($script, $context);

        $question = $this->decider->decideEntry($script);
        if (null === $question) {
            throw new LogicException('Entry point for script not found!');
        }

        $queryResult = $this->createQuestionResult($scriptResult, $question);

        $scriptResult->addQuestionResult($queryResult);

        $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_BEFORE_START, new ScriptEvent($context, $scriptResult));
        $this->storage->persist($scriptResult);

        $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_STARTED, new ScriptEvent($context, $scriptResult));

        return $scriptResult;
    }

    /**
     * @param ScriptResultInterface $scriptResult
     */
    public function restart(ScriptResultInterface $scriptResult)
    {
        $scriptResult->clearQuestionResults();

        $question = $this->decider->decideEntry($scriptResult->getScript());
        if (null === $question) {
            throw new LogicException('Entry point for script not found!');
        }

        $queryResult = $this->createQuestionResult($scriptResult, $question);

        $scriptResult->addQuestionResult($queryResult);

        $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_BEFORE_RESTART, new ScriptRestartEvent($scriptResult));
        $this->storage->persist($scriptResult);
        $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_RESTARTED, new ScriptRestartEvent($scriptResult));
    }

    /**
     * In accept method ScriptManager update last QueryResult by value from answer
     * find next question
     * create new QuestionResult and add them to ScriptResult
     *
     * @param AnswerInterface $answer
     * @param ScriptContextInterface $context
     */
    public function accept(AnswerInterface $answer, ScriptContextInterface $context)
    {
        $scriptResult = $this->getScriptResult($context);
        $questionResult = $scriptResult->getLastQuestionResult();
        $questionResult->setAnswer($answer);

        $this->eventDispatcher->dispatch(ScriptEvents::QUESTION_ANSWERED, new QuestionEvent($context, $scriptResult, $questionResult));

        if (null === $next = $this->decider->decide($scriptResult->getLastQuestionResult(), $context)) {
            $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_BEFORE_FINISH, new ScriptEvent($context, $scriptResult));
            $scriptResult->finish();
            $this->storage->persist($scriptResult);
            $this->eventDispatcher->dispatch(ScriptEvents::SCRIPT_FINISHED, new ScriptEvent($context, $scriptResult));

            return;
        }

        $this->eventDispatcher->dispatch(ScriptEvents::QUESTION_ANNOUNCE, new QuestionAnnounceEvent($context, $scriptResult, $next));

        $queryResult = $this->createQuestionResult($scriptResult, $next);
        $scriptResult->addQuestionResult($queryResult);
        $this->storage->persist($scriptResult);

        $this->eventDispatcher->dispatch(ScriptEvents::QUESTION_ANNOUNCED, new ScriptEvent($context, $scriptResult));
    }

    public function back(ScriptContextInterface $context) {
        $scriptResult = $this->getScriptResult($context);
        $scriptResult->removeLastQuestionResult();
        $this->storage->persist($scriptResult);
    }

    /**
     * @param ScriptInterface $script
     * @param ScriptContextInterface $context
     * @return ScriptResultInterface
     */
    abstract protected function createScriptResult(ScriptInterface $script, ScriptContextInterface $context): ScriptResultInterface;

    /**
     * @param ScriptResultInterface $scriptResult
     * @param QuestionInterface $question
     * @return QuestionResultInterface
     */
    abstract protected function createQuestionResult(ScriptResultInterface $scriptResult, QuestionInterface $question): QuestionResultInterface;

    protected function onAnswerAccept(ScriptResultInterface $scriptResult, ScriptContextInterface $context)
    {
    }

    protected function onScriptFinished(ScriptResultInterface $scriptResult, ScriptContextInterface $context)
    {
    }

    /**
     * @param ScriptContextInterface $context
     * @return ScriptResultInterface
     */
    private function getScriptResult(ScriptContextInterface $context): ScriptResultInterface
    {
        return $this->storage->restore($context);
    }
}
