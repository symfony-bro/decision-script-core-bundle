<?php

namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;

use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface BoolAnswerInterface extends AnswerInterface
{
    public function getValue(): bool;
}
