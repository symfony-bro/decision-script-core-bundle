<?php
namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;

use DateTime;
use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface DatetimeAnswerInterface extends AnswerInterface
{
    public function getValue(): DateTime;
}
