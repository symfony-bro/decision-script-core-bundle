<?php

namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;

use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface MultipleChoiceAnswerInterface extends AnswerInterface
{
    /**
     * @return string[]
     */
    public function getValue(): array;
}
