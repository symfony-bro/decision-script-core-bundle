<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;


use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface NullAnswerInterface extends AnswerInterface
{

}