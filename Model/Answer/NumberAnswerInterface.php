<?php
/**
 * @author Anikeev Dmitry <dm.anikeev@gmail.com>
 */

namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;


use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface NumberAnswerInterface extends AnswerInterface
{
    public function getValue() : float;
}