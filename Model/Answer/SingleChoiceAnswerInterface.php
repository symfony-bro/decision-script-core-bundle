<?php
namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;


use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface SingleChoiceAnswerInterface extends AnswerInterface
{
    public function getValue(): string;
}
