<?php
namespace SymfonyBro\DecisionScriptCoreBundle\Model\Answer;

use SymfonyBro\DecisionScriptCoreBundle\Model\AnswerInterface;

interface TextAnswerInterface extends AnswerInterface
{
    public function getValue(): string;
}
