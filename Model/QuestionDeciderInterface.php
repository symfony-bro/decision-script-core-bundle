<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface QuestionDeciderInterface
{
    /**
     * @param QuestionResultInterface $questionResult
     * @param ScriptContextInterface $context
     * @return null|QuestionInterface
     */
    public function decide(QuestionResultInterface $questionResult, ScriptContextInterface $context);

    /**
     * @param ScriptInterface $script
     * @return null|QuestionInterface
     */
    public function decideEntry(ScriptInterface $script);
}
