<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface QuestionInterface
{
    const TYPE_TEXT = 'text';
    const TYPE_SINGLE_CHOICE = 'single_choice';
    const TYPE_MULTIPLE_CHOICE = 'multiple_choice';
    const TYPE_BOOL = 'bool';
    const TYPE_DATETIME = 'datetime';
    const TYPE_NULL = 'null';
    const TYPE_NUMBER = 'number';

    public function getType(): string;

    public function getScript(): ScriptInterface;
}
