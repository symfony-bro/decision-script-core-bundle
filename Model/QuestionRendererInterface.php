<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface QuestionRendererInterface
{
    /**
     * @param ScriptContextInterface $scriptContext
     * @param QuestionInterface $question
     */
    public function render(ScriptContextInterface $scriptContext, QuestionInterface $question);
}
