<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface QuestionResultInterface
{
    public function getQuestion(): QuestionInterface;

    public function getValue();

    public function setAnswer(AnswerInterface $answer);

    public function getScriptResult(): ScriptResultInterface;
}
