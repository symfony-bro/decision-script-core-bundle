<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface ScriptResultInterface
{
    public function addQuestionResult(QuestionResultInterface $questionResult);

    public function getLastQuestionResult(): QuestionResultInterface;

    public function isFinished(): bool;

    public function finish();

    public function removeLastQuestionResult();

    public function clearQuestionResults();
}
