<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;

interface ScriptResultStorageInterface
{
    public function persist(ScriptResultInterface $result);

    public function restore(ScriptContextInterface $context): ScriptResultInterface;
}
