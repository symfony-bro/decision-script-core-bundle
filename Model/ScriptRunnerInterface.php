<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */


namespace SymfonyBro\DecisionScriptCoreBundle\Model;


interface ScriptRunnerInterface
{
    public function start(ScriptInterface $script, ScriptContextInterface $context): ScriptResultInterface;

    public function restart(ScriptResultInterface $scriptResult);

    public function accept(AnswerInterface $answer, ScriptContextInterface $context);
}
